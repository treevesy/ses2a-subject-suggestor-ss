import random
import csv
from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *

#def unpackDictionaryKeys(dictionary):
#    list = []
#    for key in dictionary:
#        list.append(key)
#    return list

class SubjectCategory:
    def __init__(self, id, name, subjectList):
        self.id = id
        self.name = name
        self.subList = subjectList

    def getNames(self):
        subjectNames = []
        for subject in self.subList:
            subjectNames.append(subject.name)
        return subjectNames

class Subject:
    def __init__(self, id, name, units):
        self.id = id
        self.name = name
        self.units = units

class Person:
    def __init__(self, id, firstname, surname, age, gender):
        self.id = id
        self.firstname = firstname
        self.surname = surname
        self.age = age
        self.gender = gender

class SubjectSuggestor:

    ALLOWED_SUBJECT_CATEGORIES_AMOUNT = 3
    UNITS_REQUIRED = 12

    # Attributes for profile
    firstnames = []
    surnames = []
    ages = [14, 15, 16]
    genders = []

    idCounter = 0

    checkbuttons = []
    currentCategories = []
    currentPerson = Person(0, '', '', 0, 'male')
    checkstates = []
    comboboxes = []
    labels = []
    labelValues = []

    allSubjects = []
    allSubjectsList = []
    randomSubjectList = [] 

    englishSubjects = [
        Subject(1, 'English Studies', 2),
        Subject(2, 'English Standard', 2),
        Subject(3, 'English Advanced', 2),
        Subject(4, 'English EAL/D', 2),
        Subject(5, 'English Extension 1', 1),
        Subject(6, 'English Extension 2', 1)
    ]

    mathSubjects = [
        Subject(7, 'Mathematics Standard 1', 2),
        Subject(8, 'Mathematics Standard 2', 2),
        Subject(9, 'Mathematics Advanced', 2),
        Subject(10, 'Mathematics Extension 1', 2),
        Subject(11, 'Mathematics Extension 2', 2)
    ]

    scienceSubjects = [
        Subject(12, 'Biology', 2),
        Subject(13, 'Chemistry', 2),
        Subject(14, 'Earth and Environmental Science', 2),
        Subject(15, 'Investigating Science', 2),
        Subject(16, 'Physics', 2),
        Subject(17, 'Science Extension', 1)
    ]

    hsieSubjects = [
        Subject(18, 'Aboriginal Studies', 2),
        Subject(19, 'Ancient History', 2),
        Subject(20, 'Business Studies', 2),
        Subject(21, 'Economics', 2),
        Subject(22, 'Geography', 2),
        Subject(23, 'Legal Studies', 2),
        Subject(24, 'Modern History', 2),
        Subject(25, 'History Extension', 1),
        Subject(26, 'Society and Culture', 2),
        Subject(27, 'Studies of Religion I', 1),
        Subject(28, 'Studies of Religion II', 2)
    ]

    technologySubjects = [
        Subject(29, 'Agriculture', 2),
        Subject(30, 'Design and Technology', 2),
        Subject(31, 'Engineering Studies', 2),
        Subject(32, 'Food Technology', 2),
        Subject(33, 'Industrial Technology', 2),
        Subject(34, 'Information Processes and Technology', 2),
        Subject(35, 'Software Design and Development', 2),
        Subject(36, 'Textiles and Design', 2)
    ]

    creativeArtsSubjects = [
        Subject(37, 'Dance', 2),
        Subject(38, 'Drama', 2),
        Subject(39, 'Music 1', 2),
        Subject(40, 'Music 2', 2),
        Subject(41, 'Music Extension', 1),
        Subject(42, 'Visual Arts', 2)
    ]

    pdhpeSubjects = [
        Subject(41, 'Community and Family Studies', 2),
        Subject(42, 'Personal Development, Health and Physical Education', 2)
    ]

    subjectCategoriesAndSubjects = [
        SubjectCategory(1, 'English', englishSubjects),
        SubjectCategory(2, 'Mathematics', mathSubjects),
        SubjectCategory(3, 'Science', scienceSubjects),
        SubjectCategory(4, 'Technologies', technologySubjects),
        SubjectCategory(5, 'Human Society and Its Environment', hsieSubjects),
        SubjectCategory(6, 'Creative Arts', creativeArtsSubjects),
        SubjectCategory(6, 'Personal Development, Health and Physical Education', pdhpeSubjects)
    ]

    addCoreRequisiteRule = {
        'English Extension 1': 'English Advanced',
        'English Extension 2': 'English Extension 1',
        'Mathematics Extension 1': 'Mathematics Advanced',
        'Mathematics Extension 2': 'Mathematics Extension 1',
        'Science Extension': 'ANY SCIENCE',
        'History Extension': 'ANY HISTORY',
        'Music Extension': 'Music 2'
    }

    def __init__(self):
        self.readInData()
        idCounter = self.howManyExistingRows()
        allSubjects = allSubjects()
        allSubjectsList = sorted(allSubjectsNames())

    def allSubjects(self):
        allSubjects = []
        for subjectCategory in subjectCategoriesAndSubjects:
            for subject in subjectCategory.subList:
                allSubjects.append(subject)
        return allSubjects

    def allSubjectsNames(self):
        allSubjects = []
        for subjectCategory in subjectCategoriesAndSubjects:
            for subject in subjectCategory.subList:
                allSubjects.append(subject.name)
        return allSubjects

    def randomizeCheckedCategories(self):
        global randomSubjectList
        subjectCategoriesSelected = []
        subjectCategoriesObjects = []
        
        subjectCategoriesAndSubjectsCopy = list.copy(subjectCategoriesAndSubjects)
        for i in range(ALLOWED_SUBJECT_CATEGORIES_AMOUNT):
            randomCategoryChoice = random.choice(subjectCategoriesAndSubjectsCopy)
            subjectCategoriesAndSubjectsCopy.remove(randomCategoryChoice)
            subjectCategoriesSelected.append(randomCategoryChoice.name)
            subjectCategoriesObjects.append(randomCategoryChoice)

        randomSubjectsAdd = random.randint(5, 6)

        randomSubjectList = []
        subjectListToPickFrom = []
        for subjectCategory in subjectCategoriesObjects:
            if subjectCategory.name == 'Mathematics':
                mathSubject = subjectCategory.subList[random.randint(0, len(subjectCategory.subList)-1)].name
                randomSubjectList.append(mathSubject)
                if mathSubject == 'Mathematics Extension 1':
                    randomSubjectList.append('Mathematics Advanced')
                if mathSubject == 'Mathematics Extension 2':
                    randomSubjectList.append('Mathematics Advanced')
                    randomSubjectList.append('Mathematics Extension 1')
            elif subjectCategory.name == 'English':
                englishSubject = subjectCategory.subList[random.randint(0, len(subjectCategory.subList)-1)].name
                randomSubjectList.append(englishSubject)
                if englishSubject == 'English Extension 1':
                    randomSubjectList.append('English Advanced')
                if englishSubject == 'English Extension 2':
                    randomSubjectList.append('English Advanced')
                    randomSubjectList.append('English Extension 1')
            else:
                subjectListToPickFrom.extend(subjectCategory.subList)

        unitsAccumulated = calculateUnits(randomSubjectList)
        while len(randomSubjectList) < randomSubjectsAdd or unitsAccumulated < UNITS_REQUIRED:
            if len(subjectListToPickFrom) != 0:
                subject = subjectListToPickFrom[random.randint(0, len(subjectListToPickFrom)-1)].name
            else:
                subjectListToPickFrom.extend(scienceSubjects)
                subjectListToPickFrom.extend(hsieSubjects)
                subjectListToPickFrom.extend(technologySubjects)
                subject = subjectListToPickFrom[random.randint(0, len(subjectListToPickFrom)-1)].name
                
            randomSubjectList.append(subject)
            subjectListToPickFrom.remove(findSubjectFromName(subject))
            if subject == 'Music 1':
                subjectListToPickFrom.remove(findSubjectFromName('Music 2'))
                subjectListToPickFrom.remove(findSubjectFromName('Music Extension'))
            elif subject == 'Music 2':
                subjectListToPickFrom.remove(findSubjectFromName('Music 1'))
                subjectListToPickFrom.remove(findSubjectFromName('Music Extension'))
                ## print(subjectListToPickFrom)
                #subjectListToPickFrom.remove(findSubjectFromName('Music Extension'))
            elif subject == 'Music Extension':
                randomSubjectList.append('Music 2')
                subjectListToPickFrom.remove(findSubjectFromName('Music 2'))
                subjectListToPickFrom.remove(findSubjectFromName('Music 1'))
            elif subject == 'History Advanced':
                ancientOrModern = random.randint(0, 1)
                if ancientOrModern == 0:
                    randomSubjectList.append('Ancient History')
                    subjectListToPickFrom.remove(findSubjectFromName('Ancient History'))
                else:
                    randomSubjectList.append('Modern History')
                    subjectListToPickFrom.remove(findSubjectFromName('Modern History'))
            elif subject == 'Science Extension':
                if checkIfScienceSubject(randomSubjectList) == False:
                    scienceSubject = addScienceSubjectNotInList(randomSubjectList)
                    randomSubjectList.append(scienceSubject)
                    subjectListToPickFrom.remove(findSubjectFromName(scienceSubject))
                    
            unitsAccumulated = calculateUnits(randomSubjectList)

        #print(randomSubjectList)

        for subject in randomSubjectList:
            ## print(subject)
            addSubjectSelectionWithCurrent(indexOf(subject))
        
        #print(randomSubjectsAdd)
        return subjectCategoriesSelected

    def indexOf(self, subjectName):
        index = 0
        for subject in allSubjects:
            if subject.name == subjectName:
                return index
            else:
                index +=1
        return -1

    def calculateUnits(self, subjectList):
        unitsAccumulated = 0
        for subject in subjectList:
            unitsAccumulated += findSubjectFromName(subject).units
        return unitsAccumulated

    def checkIfScienceSubject(self, subjectList):
        for subject in subjectList:
            if findSubjectFromName(subject) in scienceSubjects:
                return True
        return False

    def addScienceSubjectNotInList(self, subjectList):
        duplicateScienceSubjects = list.copy(scienceSubjects)
        duplicateScienceSubjects.remove(findSubjectFromName('Science Extension'))
        return duplicateScienceSubjects[random.randint(0, len(duplicateScienceSubjects)-1)].name

    def removeSubjectSelection(self):
        comboboxesCount = len(comboboxes)
        if(comboboxesCount != 0):
            comboboxesCount -= 1
            combobox = comboboxes[comboboxesCount]
            comboboxes.remove(combobox)
            combobox.destroy()
            ## print(comboboxes)
        else:
            print('no comboboxes to remove currently')
            print(comboboxes, comboboxesCount)

    def addSubjectSelectionWithCurrent(self, current):
        comboboxesCount = len(comboboxes)
        comboboxes.append(Combobox(window, values = allSubjectsList))
        comboboxes[comboboxesCount].current(current)
        comboboxes[comboboxesCount].grid(column = 0, row = 6 + comboboxesCount) 

    def addSubjectSelection(self):
        comboboxesCount = len(comboboxes)
        comboboxes.append(Combobox(window, values = allSubjectsList))
        comboboxes[comboboxesCount].current(0)
        comboboxes[comboboxesCount].grid(column = 0, row = 8 + comboboxesCount) 

    def removeAll(self, tkinterList):
        for tkElement in tkinterList:
            tkElement.destroy()
        tkinterList.clear()

    def printComboboxes(self):
        print(comboboxes)

    def randomizeInputData(self):
        global currentCategories
        global comboboxes
        global checkbuttons
        global labels
        
        removeAll(checkbuttons)
        # removeAll(checkstates)
        removeAll(comboboxes)
        removeAll(labels)
        checkstates.clear()
        labelValues.clear()
        #print(labels)

        personChoice = random.randint(0, len(firstnames)-1)
        firstname = firstnames[personChoice]
        surname = random.choice(surnames)
        age = random.choice(ages)
        gender = genders[personChoice]
        firstnameLbl = Label(window, text = firstname)
        firstnameLbl.grid(column = 1, row = 2)
        surnameLbl = Label(window, text = surname)
        surnameLbl.grid(column = 3, row = 2)
        ageLbl = Label(window, text = age)
        ageLbl.grid(column = 5, row = 2)
        genderLbl = Label(window, text = gender)
        genderLbl.grid(column = 7, row = 2)

        labels.append(firstnameLbl)
        labels.append(surnameLbl)
        labels.append(ageLbl)
        labels.append(genderLbl)

        labelValues.append(firstname)
        labelValues.append(surname)
        labelValues.append(age)
        labelValues.append(gender)

        currentCategories = randomizeCheckedCategories()
        
        i = 0
        for subjectCategory in subjectCategoriesAndSubjects:
            checkstates.append(BooleanVar())
            if(subjectCategory.name in currentCategories):
                checkstates[i].set(True)
            else:
                checkstates[i].set(False)
            checkbuttons.append(Checkbutton(window, text=subjectCategory.name, var=checkstates[i], state=DISABLED))
            checkbuttons[i].grid(column=i, row=4)
            i += 1

    def findSubjectFromName(self, subjectName):
        for subject in allSubjects:
            if subjectName == subject.name:
                return subject
        messagebox.showerror('Subject Error', 'Subject not found...')
        return Subject(0, 'Not Found', 0)

    def exportSubjectsAndUnits(self, person, subjectCategories, subjectList, units):
        global subjectCategoriesSelected
        with open('preferenceData.csv', 'a', encoding='UTF8', newline='') as csvfile:
            writer = csv.writer(csvfile)
            row = [
                person.id,
                person.firstname,
                person.surname,
                person.age,
                person.gender,
                units,
                len(subjectList),
                subjectCategories[0],
                subjectCategories[1],
                subjectCategories[2],
            ]
            for subject in subjectList:
                row.append(subject)
            writer.writerow(row)

    def addData(self):
        global idCounter
        global randomSubjectList
        ## for value in labelValues:
            ## print(value)
        
        subjectList = []
        for combobox in comboboxes:
            subjectName = combobox.get()
            subjectList.append(findSubjectFromName(subjectName))

        units = 0
        for subject in randomSubjectList:
            units += findSubjectFromName(subject).units


        if units >= UNITS_REQUIRED:
            print(idCounter)
            idCounter += 1
            exportSubjectsAndUnits(Person(idCounter, labelValues[0], labelValues[1], labelValues[2], labelValues[3]), currentCategories, randomSubjectList, units)
            randomizeInputData()
        else:
            messagebox.showerror('Units error', 'Not enough units selected!\nOnly ' + str(units) + ' units have been selected...\nPlease select ' + str(UNITS_REQUIRED-units) + ' more units and try again')

    def readInData(self):
        with open('5000-Records.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                firstnames.append(row['First Name'])
                surnames.append(row['Last Name'])
                genders.append(row['Gender'])

    def howManyExistingRows(self):
        with open('preferenceData.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            return len(list(reader))

def main():
    ss = SubjectSuggestor()
    
    window = Tk()
    window.columnconfigure(0, weight=125)
    window.columnconfigure(1, weight=125)
    window.columnconfigure(2, weight=125)
    window.columnconfigure(3, weight=125)
    window.columnconfigure(4, weight=125)
    window.columnconfigure(5, weight=125)
    window.columnconfigure(6, weight=125)
    window.columnconfigure(7, weight=125)
    window.title('Random student data generator app!')
    window.geometry('1100x400')

    randomizeButton = Button(window, text = 'Randomise Input', command = ss.randomizeInputData)
    randomizeButton.grid(column=0, row=0)

    addButton = Button(window, text = 'Add Subject', command = ss.addSubjectSelection)
    addButton.grid(column=1, row=0)

    removeButton = Button(window, text = 'Remove Subject', command = ss.removeSubjectSelection)
    removeButton.grid(column = 2, row = 0)  

    addDataButton = Button(window, text = 'Add Data', command = ss.addData)
    addDataButton.grid(column=3, row=0)

    exitButton = Button(window, text = 'Exit Program', command = lambda: window.destroy())
    exitButton.grid(column=4, row=0)

    profileLbl = Label(window, text = 'Person Profile')
    profileLbl.grid(column = 0, row = 1)

    profileFirstnameLbl = Label(window, text = 'Firstname: ')
    profileFirstnameLbl.grid(column = 0, row = 2)

    profileSurnameLbl = Label(window, text = 'Surname: ')
    profileSurnameLbl.grid(column = 2, row = 2)

    profileAgeLbl = Label(window, text = 'Age: ')
    profileAgeLbl.grid(column = 4, row = 2)

    profileGenderLbl = Label(window, text = 'Gender: ')
    profileGenderLbl.grid(column = 6, row = 2)

    courseCategoryLbl = Label(window, text = 'Course Categories')
    courseCategoryLbl.grid(column = 0, row = 3)

    courseSelectionLbl = Label(window, text = 'Subject Selection')
    courseSelectionLbl.grid(column = 0, row = 5)

    #printComboboxesButton = Button(window, text = 'print all comboboxes', command = printComboboxes)
    #printComboboxesButton.grid(column=1, row=2)

    #randomizeInputData()
    #for i in range(100000):
        #addData()

    window.mainloop()

main()
