import random
import csv
from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *

class SubjectCategory:
    def __init__(self, id, name, subjectList):
        self.id = id
        self.name = name
        self.subList = subjectList

    def getNames(self):
        subjectNames = []
        for subject in self.subList:
            subjectNames.append(subject.name)
        return subjectNames

class Subject:
    def __init__(self, id, name, units):
        self.id = id
        self.name = name
        self.units = units

class Person:
    def __init__(self, id, firstname, surname, age, gender):
        self.id = id
        self.firstname = firstname
        self.surname = surname
        self.age = age
        self.gender = gender

class PersonRandomData:
    firstnames = []
    surnames = []
    ages = [14, 15, 16, 17]
    genders = []

    def __init__(self, firstnames, surnames, genders):
        self.firstnames = firstnames
        self.surnames = surnames
        self.genders = genders

    def getFirstnames(self):
        return self.firstNames

    def getSurnames(self):
        return self.surnames

    def getGenders(self):
        return self.genders

class FileHandler:
    def __init__(self):
        self.ROW_COUNT = self.howManyExistingRows()

    def getRowCount(self):
        return self.ROW_COUNT
    
    def exportSubjectsAndUnits(
            self,
            topCategories,
            electiveList,
            interestStarList,
            performanceStarList,
            subjectList,
            randomPerson
        ):
        with open('preferenceData.csv', 'a', encoding='UTF8', newline='') as csvfile:
            writer = csv.writer(csvfile)
            row = [
                self.ROW_COUNT,
                randomPerson.firstname,
                randomPerson.surname,
                randomPerson.age,
                randomPerson.gender,
                topCategories[0],
                topCategories[1],
                topCategories[2],
                electiveList[0].name,
                electiveList[1].name,
                electiveList[2].name,
                performanceStarList[0],
                interestStarList[0],
                performanceStarList[1],
                interestStarList[1],
                performanceStarList[2],
                interestStarList[2],
                performanceStarList[3],
                interestStarList[3],
                performanceStarList[4],
                interestStarList[4],
                performanceStarList[5],
                interestStarList[5],
                performanceStarList[6],
                interestStarList[6]
            ]

            # WRITE ELECTIVE PERFORMANCE AND INTEREST
            for subjectCategory in yearTenSubjectCategoriesAndElectives:
                for elective in subjectCategory.subList:
                    inList = False
                    for i in range(len(electiveList)):
                        if elective.name == electiveList[i].name and inList == False:
                            row.append(performanceStarList[7+i])
                            row.append(interestStarList[7+i])
                            inList = True
                    if inList == False:
                        row.append(None)
                        row.append(None)

            # WRITE SUBJECTS CHOSEN - OUTPUT
            for subjectCategory in subjectCategoriesAndSubjects:
                for subject in subjectCategory.subList:
                    inList = False
                    for i in range(len(subjectList)):
                        if subject.name == subjectList[i] and inList == False:
                            row.append(1)
                            inList = True
                    if inList == False:
                        row.append(0)
                        
            writer.writerow(row)
            self.ROW_COUNT += 1
        return True

    def readInData(self):
        firstnames = []
        surnames = []
        genders = []
        with open('5000-Records.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                firstnames.append(row['First Name'])
                surnames.append(row['Last Name'])
                genders.append(row['Gender'])
        return PersonRandomData(firstnames, surnames, genders)

    def howManyExistingRows(self):
        with open('preferenceData.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            return len(list(reader))
        

class SubjectSuggestor:
    #STATIC VARIABLES FOR SUBJECT SUGGESTOR
    ALLOWED_SUBJECT_CATEGORIES_AMOUNT = 3
    UNITS_REQUIRED = 10
    FILE_HANDLER = FileHandler()

    #LISTS TO USE FOR SUBJECT SUGGESTOR
    currentCategories = []
    # allSubjects = []
    # allSubjectsList = []
    randomSubjectList = []

    def __init__(self):
        #Person Random Data read in from file
        self.randomPersonData = self.FILE_HANDLER.readInData()
        self.allSubjects = self.allSubjects()
        self.allSubjectsList = sorted(self.allSubjectsNames())
        self.allYrTenElectives = self.allElectives()

    def allSubjects(self):
        allSubjects = []
        for subjectCategory in subjectCategoriesAndSubjects:
            for subject in subjectCategory.subList:
                allSubjects.append(subject)
        return allSubjects

    def allElectives(self):
        allElectives = []
        for subjectCategory in yearTenSubjectCategoriesAndElectives:
            for elective in subjectCategory.subList:
                allElectives.append(elective)
        return allElectives

    def getThreeElectives(self):
        electiveList = []
        allYrTenElectivesCopy = list.copy(self.allYrTenElectives)
        for i in range(3):
            randomElectiveChoice = random.choice(allYrTenElectivesCopy)
            allYrTenElectivesCopy.remove(randomElectiveChoice)
            electiveList.append(randomElectiveChoice)
        return electiveList
        

    def allSubjectsNames(self):
        allSubjects = []
        for subjectCategory in subjectCategoriesAndSubjects:
            for subject in subjectCategory.subList:
                allSubjects.append(subject.name)
        return allSubjects

    def addData(self,
                topCategories,
                randomElectiveList,
                interestStarList,
                performanceStarList,
                subjectList,
                randomPerson
            ):

        units = 0
        for subject in subjectList:
            units += self.findSubjectFromName(subject).units


        if units >= self.UNITS_REQUIRED:
            return self.FILE_HANDLER.exportSubjectsAndUnits(
                topCategories,
                randomElectiveList,
                interestStarList,
                performanceStarList,
                subjectList,
                randomPerson
            )
        else:
            messagebox.showerror('Units error', 'Not enough units selected!\nOnly ' + str(units) + ' units have been selected...\nPlease select ' + str(UNITS_REQUIRED-units) + ' more units and try again')

    def findSubjectFromName(self, subjectName):
        for subject in self.allSubjects:
            if subject.name == subjectName:
                return subject
    
    def removeAll(self, tkinterList):
        for tkElement in tkinterList:
            tkElement.destroy()
        tkinterList.clear()

    def removeSubjectSelection(self):
        comboboxesCount = len(comboboxes)
        if(comboboxesCount != 0):
            comboboxesCount -= 1
            combobox = comboboxes[comboboxesCount]
            comboboxes.remove(combobox)
            combobox.destroy()
        else:
            print('no comboboxes to remove currently')
            print(comboboxes, comboboxesCount)

    def randomPerson(self):
        personChoice = random.randint(0, len(self.randomPersonData.firstnames)-1)
        firstname = self.randomPersonData.firstnames[personChoice]
        surname = random.choice(self.randomPersonData.surnames)
        age = random.choice(self.randomPersonData.ages)
        gender = self.randomPersonData.genders[personChoice]
        return Person(self.FILE_HANDLER.ROW_COUNT, firstname, surname, age, gender)

    def randomizeCheckedCategories(self):
        subjectCategoriesSelected = []
        subjectCategoriesObjects = []
        
        subjectCategoriesAndSubjectsCopy = list.copy(subjectCategoriesAndSubjects)
        for i in range(self.ALLOWED_SUBJECT_CATEGORIES_AMOUNT):
            randomCategoryChoice = random.choice(subjectCategoriesAndSubjectsCopy)
            subjectCategoriesAndSubjectsCopy.remove(randomCategoryChoice)
            subjectCategoriesSelected.append(randomCategoryChoice.name)
        return subjectCategoriesSelected

    def randomizeInputPerson(self): 
        person = self.randomPerson()
        return person

class tkinterViewHandler:
    subjectCategoryCheckButtons = []
    currentCategories = []
    labels = []
    labelValues = []
    checkstates = []
    finalSubjectSelections = []

    rows = []

    class Row:
        
        def __init__(self, rowNumber, elements, positions=None):
            self.elements = []
            self.rowNumber = rowNumber
            
            i = 0
            for element in elements:
                if positions != None:
                    element.grid(column = positions[i], row = self.rowNumber)
                else:
                    element.grid(column = i, row = self.rowNumber)
                self.elements.append(element)
                i += 1

        def addElement(self, element):
            self.elements.append(element)

        def removeAllElements(self):
            for element in self.elements:
                element.destroy()
            self.elements.clear()

        def getElements(self):
            return self.elements

    def __init__(self):
        self.window = Tk()
        # self.window.attributes('-fullscreen', True)
        self.ss = SubjectSuggestor()
        self.initialSetup()
        self.randomizeInputData()

    def addRow(self, rowNumber, elements):
        self.rows.append(
            self.Row(
                rowNumber,
                elements
            )
        )

    def addRowWithPositions(self, rowNumber, elements, positions):
        self.rows.append(
            self.Row(
                rowNumber,
                elements,
                positions
            )
        )

    def getRow(self, rowNumber):
        for row in self.rows:
            if row.rowNumber == rowNumber:
                return row
        return None

    def removeRow(self, rowNumber):
        for row in self.rows:
            if row.rowNumber == rowNumber:
                row.removeAllElements()
                self.rows.remove(row)
                return None

    def addSubjectSelectionWithCurrent(self, current):
        subjectSelectionsCount = len(self.finalSubjectSelections)
        self.finalSubjectSelections.append(Combobox(self.window, values = self.ss.allSubjectsList))
        self.finalSubjectSelections[subjectSelectionsCount].current(current)
        self.finalSubjectSelections[subjectSelectionsCount].grid(column = 0, row = 19 + subjectSelectionsCount)

    def addSubjectSelection(self):
        self.addSubjectSelectionWithCurrent(0)
        
    def randomizeInputData(self):

        # EXTRA ROW 2
        self.removeRow(2)

        self.person = self.ss.randomizeInputPerson()
        firstnameLbl = Label(self.window, text = self.person.firstname)
        surnameLbl = Label(self.window, text = self.person.surname)
        ageLbl = Label(self.window, text = self.person.age)
        genderLbl = Label(self.window, text = self.person.gender)

        self.addRowWithPositions(
            2,
            [
                firstnameLbl,
                surnameLbl,
                ageLbl,
                genderLbl
            ],
            [1, 3, 5, 7]
        )
 
        #ROW 4 GET RANDOM CATEGORIES INTERESTED IN
        self.checkstates.clear()
        self.subjectCategoriesSelected = self.ss.randomizeCheckedCategories()

        categoriesCheckboxes = []
        i = 0
        for subjectCategory in subjectCategoriesAndSubjects:
            self.checkstates.append(BooleanVar())
            if(subjectCategory.name in self.subjectCategoriesSelected):
                self.checkstates[i].set(True)
            else:
                self.checkstates[i].set(False)
            categoriesCheckboxes.append(Checkbutton(self.window, text=subjectCategory.name, var=self.checkstates[i], state=DISABLED))
            i += 1

        #ROW 4
        self.removeRow(4)
        self.addRow(
            4,
            categoriesCheckboxes
        )

        #ROW 6
        self.removeRow(6)
        self.threeElectives = self.ss.getThreeElectives()

        yrTenSubjectLabels = []
        for elective in self.threeElectives:
            yrTenSubjectLabels.append(Label(self.window, text=elective.name))
            
        self.addRow(
            6,
            yrTenSubjectLabels
        )

        # ROW 8
        self.removeRow(8)
        categoryPerformanceLabels = [
            Label(self.window, text='English'),
            Label(self.window, text='Mathematics'),
            Label(self.window, text='Science'),
            Label(self.window, text='HSIE'),
            Label(self.window, text='PDHPE'),
            Label(self.window, text='Creative Arts'),
            Label(self.window, text='Technologies')
        ]
        self.addRow(
            8,
            categoryPerformanceLabels
        )

        # ROW 9
        self.removeRow(9)
        self.interestStars = []
        categoryInterestStarLabels = []
        for i in range(7):
            self.interestStars.append(str(random.randint(0,5)))
            categoryInterestStarLabels.append(Label(self.window, text=self.interestStars[i]))
        self.addRow(
            9,
            categoryInterestStarLabels
        )

        # ROW 11
        self.removeRow(11)
        electiveInterestLabels = []
        electiveInterestLabels.append(Label(self.window, text=self.threeElectives[0].name))
        electiveInterestLabels.append(Label(self.window, text=self.threeElectives[1].name))
        electiveInterestLabels.append(Label(self.window, text=self.threeElectives[2].name))
        self.addRow(
            11,
            electiveInterestLabels
        )

        # ROW 12
        self.removeRow(12)
        electiveInterestStarLabels = []
        for i in range(7, 10):
            self.interestStars.append(str(random.randint(0,5)))
            electiveInterestStarLabels.append(Label(self.window, text=self.interestStars[i]))
        self.addRow(
            12,
            electiveInterestStarLabels
        )

        # ROW 14
        self.removeRow(14)
        categoryPerformanceLabels = [
            Label(self.window, text='English'),
            Label(self.window, text='Mathematics'),
            Label(self.window, text='Science'),
            Label(self.window, text='HSIE'),
            Label(self.window, text='PDHPE'),
            Label(self.window, text='Creative Arts'),
            Label(self.window, text='Technologies')
        ]
        self.addRow(
            14,
            categoryPerformanceLabels
        )

        # ROW 15
        self.removeRow(15)
        self.performanceStars = []
        categoryPerformaceStarLabels = []
        for i in range(7):
            self.performanceStars.append(str(random.randint(0,5)))
            categoryPerformaceStarLabels.append(Label(self.window, text=self.performanceStars[i]))
        self.addRow(
            15,
            categoryPerformaceStarLabels
        )

        # ROW 17
        self.removeRow(17)
        electivePerformanceLabels = []
        electivePerformanceLabels.append(Label(self.window, text=self.threeElectives[0].name))
        electivePerformanceLabels.append(Label(self.window, text=self.threeElectives[1].name))
        electivePerformanceLabels.append(Label(self.window, text=self.threeElectives[2].name))
        self.addRow(
            17,
            electivePerformanceLabels
        )

        # ROW 18
        self.removeRow(18)
        electiveInterestStarLabels = []
        for i in range(7, 10):
            self.performanceStars.append(str(random.randint(0,5)))
            electiveInterestStarLabels.append(Label(self.window, text=self.performanceStars[i]))
        self.addRow(
            18,
            electiveInterestStarLabels
        )
        
        # ROW 19 AND ONWARDS
        self.removeAllSubjectSelectionComboboxes()
        self.finalSubjectSelections.clear()
        for i in range(6):
            self.addSubjectSelection()

    def getSubjectSelectionFromComboboxes(self, subjectComboboxList):
        subjectSelectionList = []
        for i in range(len(subjectComboboxList)):
            subjectSelectionList.append(subjectComboboxList[i].get())
        return subjectSelectionList

    def removeAllSubjectSelectionComboboxes(self):
        while len(self.finalSubjectSelections) != 0:
            self.removeSubjectSelection()

    def removeSubjectSelection(self):
        combobox = self.finalSubjectSelections[len(self.finalSubjectSelections)-1]
        self.finalSubjectSelections.remove(combobox)
        combobox.destroy()

    def addData(self):
        if self.ss.addData(
            self.subjectCategoriesSelected,
            self.threeElectives,
            self.interestStars,
            self.performanceStars,
            self.getSubjectSelectionFromComboboxes(self.finalSubjectSelections),
            self.person
        ):
            self.randomizeInputData()
        else:
            messagebox.showerror('There was an error saving the data')

    def quit(self):
        self.root.destroy()

    def initialSetup(self):
        self.window.title('Random student data generator app!')
        self.window.geometry('1100x400')
        
        self.window.columnconfigure(0, weight=100)
        self.window.columnconfigure(1, weight=100)
        self.window.columnconfigure(2, weight=100)
        self.window.columnconfigure(3, weight=100)
        self.window.columnconfigure(4, weight=100)
        self.window.columnconfigure(5, weight=100)
        self.window.columnconfigure(6, weight=100)
        self.window.columnconfigure(7, weight=100)
        self.window.columnconfigure(8, weight=100)

        #ROW 0
        self.addRow(
            0,
            [
                Button(self.window, text = 'Randomise Input', command = self.randomizeInputData),
                Button(self.window, text = 'Add Subject', command = self.addSubjectSelection),
                Button(self.window, text = 'Remove Subject', command = self.removeSubjectSelection),
                Button(self.window, text = 'Add Data', command = self.addData),
                Button(self.window, text = 'Exit Program', command = self.window.destroy)
            ]
        )
           
        #ROW 1
        profileLbl = Label(self.window, text = 'Person Profile')
        profileLbl.grid(column = 0, row = 1)

        #ROW 2
        profileFirstnameLbl = Label(self.window, text = 'Firstname: ')
        profileSurnameLbl = Label(self.window, text = 'Surname: ')
        profileAgeLbl = Label(self.window, text = 'Age: ')
        profileGenderLbl = Label(self.window, text = 'Gender: ')
        profileFirstnameLbl.grid(column = 0, row = 2)
        profileSurnameLbl.grid(column = 2, row = 2)
        profileAgeLbl.grid(column = 4, row = 2)
        profileGenderLbl.grid(column = 6, row = 2)

        # ROW 3
        courseCategoryLbl = Label(self.window, text = 'Course Categories')
        courseCategoryLbl.grid(column = 0, row = 3)

        # ROW 5
        self.addRow(
            5,
            [
                Label(self.window, text='Yr10 Elective Selection')
            ]
        )

        # ROW 7
        self.addRow(
            7,
            [
                Label(self.window, text='Interest in categories (0-5 stars)')
            ]
        )

        # ROW 10
        self.addRow(
            10,
            [
                Label(self.window, text='Interest in electives (0-5 stars)')
            ]
        )

        # ROW 8
##        self.addRow(
##            8,
##            [
##                Label(self.window, text='English'),
##                Label(self.window, text='Mathematics'),
##                Label(self.window, text='Science'),
##                Label(self.window, text='Human Society and Its environment'),
##                Label(self.window, text='Personal Development, Health and Physical Education')
##            ]
##        )

        # ROW 13
        self.addRow(
            13,
            [
                Label(self.window, text='Performance in categories (0-5 stars)')
            ]
        )

        # ROW 16
        self.addRow(
            16,
            [
                Label(self.window, text='Performance in electives (0-5 stars)')
            ]
        )

        # ROW 11
##        self.addRow(
##            11,
##            [
##                Label(self.window, text='English'),
##                Label(self.window, text='Mathematics'),
##                Label(self.window, text='Science'),
##                Label(self.window, text='Human Society and Its environment'),
##                Label(self.window, text='Personal Development, Health and Physical Education')
##            ]
##        )


        self.window.mainloop()

def main():
    tkinterViewHandler()

#SUBJECT LISTS TO USE FOR PROGRAM
englishSubjects = [
    Subject(1, 'English Studies', 2),
    Subject(2, 'English Standard', 2),
    Subject(3, 'English Advanced', 2),
    Subject(4, 'English EAL/D', 2),
    Subject(5, 'English Extension 1', 1),
    Subject(6, 'English Extension 2', 1)
]

mathSubjects = [
    Subject(7, 'Mathematics Standard 1', 2),
    Subject(8, 'Mathematics Standard 2', 2),
    Subject(9, 'Mathematics Advanced', 2),
    Subject(10, 'Mathematics Extension 1', 2),
    Subject(11, 'Mathematics Extension 2', 2)
]

scienceSubjects = [
    Subject(12, 'Biology', 2),
    Subject(13, 'Chemistry', 2),
    Subject(14, 'Earth and Environmental Science', 2),
    Subject(15, 'Investigating Science', 2),
    Subject(16, 'Physics', 2),
    Subject(17, 'Science Extension', 1)
]

hsieSubjects = [
    Subject(18, 'Aboriginal Studies', 2),
    Subject(19, 'Ancient History', 2),
    Subject(20, 'Business Studies', 2),
    Subject(21, 'Economics', 2),
    Subject(22, 'Geography', 2),
    Subject(23, 'Legal Studies', 2),
    Subject(24, 'Modern History', 2),
    Subject(25, 'History Extension', 1),
    Subject(26, 'Society and Culture', 2),
    Subject(27, 'Studies of Religion I', 1),
    Subject(28, 'Studies of Religion II', 2)
]

technologySubjects = [
    Subject(29, 'Agriculture', 2),
    Subject(30, 'Design and Technology', 2),
    Subject(31, 'Engineering Studies', 2),
    Subject(32, 'Food Technology', 2),
    Subject(33, 'Industrial Technology', 2),
    Subject(34, 'Information Processes and Technology', 2),
    Subject(35, 'Software Design and Development', 2),
    Subject(36, 'Textiles and Design', 2)
]

creativeArtsSubjects = [
    Subject(37, 'Dance', 2),
    Subject(38, 'Drama', 2),
    Subject(39, 'Music 1', 2),
    Subject(40, 'Music 2', 2),
    Subject(41, 'Music Extension', 1),
    Subject(42, 'Visual Arts', 2)
]

pdhpeSubjects = [
    Subject(43, 'Community and Family Studies', 2),
    Subject(44, 'Personal Development, Health and Physical Education', 2)
]

subjectCategoriesAndSubjects = [
    SubjectCategory(1, 'English', englishSubjects),
    SubjectCategory(2, 'Mathematics', mathSubjects),
    SubjectCategory(3, 'Science', scienceSubjects),
    SubjectCategory(4, 'Technologies', technologySubjects),
    SubjectCategory(5, 'Human Society and Its Environment', hsieSubjects),
    SubjectCategory(6, 'Creative Arts', creativeArtsSubjects),
    SubjectCategory(7, 'Personal Development, Health and Physical Education', pdhpeSubjects)
]

addCoreRequisiteRule = {
    'English Extension 1': 'English Advanced',
    'English Extension 2': 'English Extension 1',
    'Mathematics Extension 1': 'Mathematics Advanced',
    'Mathematics Extension 2': 'Mathematics Extension 1',
    'Science Extension': 'ANY SCIENCE',
    'History Extension': 'ANY HISTORY',
    'Music Extension': 'Music 2'
}
        
yearTenSubjectCompulsoryCategoriesAndSubjects = [
    SubjectCategory(10, 'English', [ Subject(101, 'Yr9/10 English', 0) ]),
    SubjectCategory(11, 'Mathematics', [ Subject(102, 'Yr9/10 Mathematics', 0) ]),
    SubjectCategory(12, 'Science', [ Subject(103, 'Yr9/10 Science', 0) ]),
    SubjectCategory(13, 'Personal Development, Health and Physical Education', [ Subject(104, 'Yr9/10 PDHPE', 0) ]),
    SubjectCategory(15, 'Human Society and Its Environment', [ Subject(105, 'Yr9/10 History', 0) ]),
]

yrTenHSIEElectives = [
    Subject(100, 'Commerce', 0),
    Subject(101, 'Elective Geography', 0),
    Subject(102, 'Elective History', 0)
]

yrTenCreativeArtsElectives = [
    Subject(103, 'Drama', 0),
    Subject(104, 'Music', 0),
    Subject(105, 'Photographic and Digital Media', 0),
    Subject(106, 'Visual Arts', 0)
]

yrTenTechnologyElectives = [
    Subject(107, 'Design and Technology', 0),
    Subject(108, 'Food Technology', 0),
    Subject(109, 'Industrial Technology - Engineering', 0),
    Subject(110, 'Industrial Technology - Timber', 0),
    Subject(111, 'Information Software and Technology', 0)
]                                                 

yearTenSubjectCategoriesAndElectives = [
    SubjectCategory(16, 'Human Society and Its Environment', yrTenHSIEElectives),
    SubjectCategory(17, 'Technologies', yrTenTechnologyElectives),
    SubjectCategory(18, 'Creative Arts', yrTenCreativeArtsElectives),
    SubjectCategory(19, 'Personal Development, Health and Physical Education', [ Subject(112, 'Physical Activity and Sports Studies', 0) ])
] 

def writeSpreadsheetHeader():
    with open('preferenceData.csv', 'a', encoding='UTF8', newline='') as csvfile:
        writer = csv.writer(csvfile)
        row = []
        for subjectCategory in yearTenSubjectCategoriesAndElectives:
            for elective in subjectCategory.subList:
                electiveName = elective.name.replace(' ', '_')
                row.append(electiveName + '_p')
                row.append(electiveName + '_i')
        for subjectCategory in subjectCategoriesAndSubjects:
            for subject in subjectCategory.subList:
                subjectName = subject.name.replace(' ', '_')
                row.append(subjectName)
        writer.writerow(row)

main()
